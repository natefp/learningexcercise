package info.androidhive.expandablelistview.learningexcercise.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements ISpinnerClicked {

    String[] _cityNamesUt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _cityNamesUt = getResources().getStringArray(R.array.CitiesImageNamesUt);

        Control c = (Control) findViewById(R.id.control);
        c.setMainActivity(this);
        c.setAdapter(R.array.CitiesFormattedUt);
    }

    public void spinnerClicked(int position) {
        ImageView image = (ImageView) findViewById(R.id.imageView1);

        if(image != null) {
            image.setImageResource(getResources().getIdentifier(_cityNamesUt[position], "drawable", getPackageName()));
        }
    }

    public void startNewActivity(View view)
    {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }
}