package info.androidhive.expandablelistview.learningexcercise.app;

import android.app.Activity;
import android.os.Bundle;

public class Activity2 extends Activity implements ISpinnerClicked {

    String[] _cityNamesCa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
//        _cityNamesCa = getResources().getStringArray(R.array.CitiesImageNamesUt);  // CitiesImageNamesUt used to use the same images as Utah

        Control c = (Control) findViewById(R.id.control2);
        c.setMainActivity(this);
        c.setAdapter(R.array.CitiesFormattedCa);
    }

    public void spinnerClicked(int position) {
//        ImageView image = (ImageView) findViewById(R.id.imageView1);
//
//        if(image != null) {
//            image.setImageResource(getResources().getIdentifier(_cityNamesCa[position], "drawable", getPackageName()));
//        }
    }
}
