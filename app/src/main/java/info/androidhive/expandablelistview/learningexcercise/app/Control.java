package info.androidhive.expandablelistview.learningexcercise.app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class Control extends LinearLayout implements Spinner.OnItemSelectedListener{

    ISpinnerClicked _ma;
    Context _context;
    Spinner _spinner;

    public Control(Context context) {
        super(context);
        init(context, null, 0);
    }

    public Control(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public Control(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        _context = context;
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.control, this);

        _spinner = (Spinner) getChildAt(1);


        if (_spinner != null) {
            _spinner.setOnItemSelectedListener(this);
        }
    }

    public void setMainActivity(ISpinnerClicked ma)
    {
        _ma = ma;
    }

    public void setAdapter(int resId)
    {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(_context, resId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if (_spinner != null) {
            _spinner.setAdapter(adapter);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(_ma != null){
            _ma.spinnerClicked(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}