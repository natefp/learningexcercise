package info.androidhive.expandablelistview.learningexcercise.app;

public interface ISpinnerClicked {

    public void spinnerClicked(int position);
}
